import random
import hashlib
import bech32
import base58
import time
import os
import multiprocessing
import sys
from Counter import Counter
from Counter import StopWatch
from fastecdsa import keys, curve
import re


privkey = random.getrandbits(256).to_bytes(
    32, byteorder="little", signed=False)
header_byte = ['02', '03']


def getPriPub():
    signingkey = keys.gen_private_key(curve.secp256k1)
    # signingkey = ecdsa.SigningKey.from_string(privkey,curve=SECP256k1)
    # verifykey = signingkey.get_verifying_key()
    verifykey = keys.get_public_key(signingkey, curve.secp256k1)
    return signingkey, verifykey


def privToWif(privkeyInt):
    signkey = '80' + hex(privkeyInt)[2:]
    try:
        hash1 = hashlib.sha256(bytes.fromhex(signkey)).digest()
    except ValueError as e:
        print(e)
        print(privkeyInt)
    hash2 = hashlib.sha256(hash1).hexdigest()
    preaddr = signkey + hash2[:8]
    wif = base58.b58encode(bytes.fromhex(preaddr)).decode()
    return wif


def ripemd160(x):
    d = hashlib.new('ripemd160')
    d.update(x)
    return d


def getSECP256():

    signingkey = keys.gen_private_key(curve.secp256k1)
    #signingkey = ecdsa.SigningKey.from_string(privkey,curve=SECP256k1)
    #verifykey = signingkey.get_verifying_key()
    verifykey = keys.get_public_key(signingkey, curve.secp256k1)
    return


def getLegacyAddress(pubkey):
    # publ_key = bytes.fromhex(f'04{getSECP256(privkey)}'
    # print(bytes.fromhex(pubkey.to_string().hex()))
    sha0 = hashlib.sha256(bytes.fromhex(pubkey.to_string().hex())).digest()
    ripe = ripemd160(sha0).digest()
    pub_a = bytes.fromhex(f'00{ripe.hex()}')
    checksum = hashlib.sha256(hashlib.sha256(pub_a).digest()).digest()[:4]
    pub_b = base58.b58encode(pub_a + checksum)
    return pub_b.decode()


def getkeyHash(pubkey):
    x_coord = pubkey.x.to_bytes(32, 'big')

    if(pubkey.y % 2 == 0):
        pubkey = bytes.fromhex(header_byte[0] + x_coord.hex())
    else:
        pubkey = bytes.fromhex(header_byte[1] + x_coord.hex())
    sha = hashlib.sha256(pubkey).digest()
    ripe = ripemd160(sha).digest()
    return ripe


def getP2S(pubkey):
    p2wpkh = bytes.fromhex(f'0014{getkeyHash(pubkey).hex()}')
    shap2w = hashlib.sha256(p2wpkh)
    ripep2w = ripemd160(shap2w.digest())
    scripthash = ripep2w.digest()
    p2sh = bytes.fromhex(f'a9{scripthash.hex()}87')
    flagged_scripthash = bytes.fromhex(f'05{scripthash.hex()}')
    checksum = hashlib.sha256(hashlib.sha256(
        flagged_scripthash).digest()).digest()[:4]
    bin_addr = flagged_scripthash + checksum
    nested_address = base58.b58encode(bin_addr).decode()
    return nested_address


def toBech32(pubkey):
    return bech32.encode('bc', 0, getkeyHash(pubkey))


def vainLookup(counter, string, ignoreCase):
    speed = 500
    #privkey = os.urandom(32)
    if(ignoreCase):
        p = re.compile(string, re.IGNORECASE)
        print(p)
    else:
        p = re.compile(string)
        print(p)
    while(True):
        signingkey = keys.gen_private_key(curve.secp256k1)
        verifykey = keys.get_public_key(signingkey, curve.secp256k1)
        addr = getP2S(verifykey)
        if counter.value() % speed == 0:
            if ignoreCase:
                print(addr.lower()[1:])
            else:
                print(addr[1:])
            speed = counter.getSpeed() * 2
            os.system(f'title {speed/2} addr/sec')
        if(p.match(addr[1:]) is not None):
            f = open(addr + ".txt", "w+")
            signkey = '80' + hex(signingkey)[2:]
            if len(signkey) % 2 != 0:
                signkey = signkey[0:2] + '0' + signkey[3:]
                print('\n\n\n\n\n\n\n\n\n\n\n\n')
                print('verifiy ' + addr)
            try:
                hash1 = hashlib.sha256(bytes.fromhex(signkey)).digest()
            except ValueError as e:
                f.write(str(e))
                print(addr)
                print(e)
                print(signkey)
                f.write(str(signkey))
                f.close()
            hash2 = hashlib.sha256(hash1).hexdigest()
            preaddr = signkey + hash2[:8]
            try:
                wif = base58.b58encode(bytes.fromhex(preaddr)).decode()
            except ValueError as e:
                print(addr)
                print(e)
                f.write(str(preaddr))
                f.close()
            f.write(wif)
            f.close()
            print(f'wif: \n{wif}')
            print(f'public key: \n{addr}\n')
            # length+=1
        counter.increment()


def useagePrint():
    print(len(args))
    print('USAGE: ')
    print(
        'python VanityGen \{threadCount\} \{StringToFindAtStart\} \{ignoreCase\}')
    print('EXAMPLE: python VanityGen 6 biom4st3r True')
    exit()


if __name__ == '__main__':
    args = sys.argv
    if(len(args) != 4):
        useagePrint()
    try:
        workers = int(args[1])
        string = str(args[2])
        if(args[3].lower() == 'true'):
            ignoreCase = True
        else:
            ignoreCase = False
    except Exception as e:
        print(e)
        useagePrint()
        exit()
    jobs = []
    counter = Counter()
    start = time.perf_counter()
    for i in range(0, workers):

        process = multiprocessing.Process(
            target=vainLookup, args=(counter, string, ignoreCase))
        jobs.append(process)

    for j in jobs:
        j.start()

    try:
        for j in jobs:
            j.join()
    except KeyboardInterrupt:
        persec = counter.value()/(time.perf_counter()-start)
        print('%.2f' % persec, end=' ')
        print('addr/sec')
        print('%.2f' % (persec/workers), end=' ')
        print('addr/thread/sec')
