import multiprocessing
import time


class Counter(object):
    def __init__(self):
        self.val = multiprocessing.RawValue('i', 0)
        self.lock = multiprocessing.Lock()
        self.start = time.perf_counter()
        self.total: int = 0

    def increment(self):
        with self.lock:
            self.val.value += 1

    def flip(self):
        with self.lock:
            self.val.value = not self.val.value

    def value(self) -> int:
        return self.val.value

    def reset(self):
        with self.lock:
            self.total += self.val.value
            self.val.value = 0
            self.start = time.perf_counter()

    def getSpeed(self) -> int:
        try:
            return int(self.value() / (time.perf_counter() - self.start)) + 1
        except ZeroDivisionError:
            return 500


class StopWatch():
    def __init__(self):
        self.val = 0

    def start(self):
        self.val = time.perf_counter()

    def stop(self):
        duration = time.perf_counter() - self.val
        self.val = time.perf_counter()
        return duration

# EOF
