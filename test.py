from multiprocessing import process
from multiprocessing.context import Process
from typing import List, Union
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.serialization import Encoding, PrivateFormat, PublicFormat
from cryptography.hazmat.primitives import hashes
import base58
import base64
import hashlib
import os
import bech32
import time
import atexit
from datetime import datetime
import re
import multiprocessing
from Counter import Counter
import threading
import sys

regex = re.compile(r'\b8l0m4st3r|\bm4st3r|(.)\1{6,}|\bcur[7t][lj]s|[0-9]{14,}|(..)\1{3,}|(...)\1{2,}|(....)\1{2,}|\ballys0n')

# https://www.twitch.tv/redriotross
BECH = ['q', 'p', 'z', 'r', 'y', '9', 'x', '8', 'g', 'f', '2', 't', 'v', 'd', 'w', '0', 's', '3', 'j', 'n', '5', '4', 'k', 'h', 'c', 'e', '6', 'm', 'u', 'a', '7', 'l']


def toBech(data: Union[bytes, int]):
    if isinstance(data, bytes):
        data = int.from_bytes(data, 'big', signed=False)
    out = []
    while data > 0:
        mod = data % 32
        out.insert(0, BECH[mod])
        data //= 32
    return ''.join(out)


sha256 = hashes.SHA256()


def checksum(data):
    for x in range(2):
        h = hashes.Hash(sha256)
        h.update(data)
        data = h.finalize()
    return data[:4]


prefix = b'\x03\x03\x00\x02\x03'


def gen_bech_public_key(publicNumbers: ec.EllipticCurvePublicNumbers) -> str:
    pub = ((b'\x02' if publicNumbers.y % 2 == 0 else b'\x03') + publicNumbers.x.to_bytes(32, 'big'))

    sha = hashes.Hash(sha256)
    sha.update(pub)
    sha = sha.finalize()

    h = hashlib.new('ripemd160')
    h.update(sha)
    h = h.digest()
    return bech32.encode('bc', 0, h)


def gen_wip_private_key(private_seed: bytes):
    preEncode = bytearray(b'\x80')
    preEncode.extend(private_seed)
    preEncode.extend(b'\x01')
    preEncode.extend(checksum(bytes(preEncode)))
    preEncode = bytes(preEncode)
    return base58.b58encode(preEncode)


# counter: Counter = Counter()


# while True:
#     private_seed: bytes = os.urandom(32)
#     privateKey: ec.EllipticCurvePrivateKey = ec.derive_private_key(int.from_bytes(private_seed, 'big', signed=False), ec.SECP256K1())

#     pubbc1 = gen_bech_public_key(privateKey.public_key().public_numbers())
#     match = regex.match(pubbc1[4:])
#     if match:
#         print('Found one!')
#         print(match)
#         privateWip = gen_wip_private_key(private_seed)
#         with open(f'{pubbc1}.key', 'w+') as file:
#             file.write(f'pub: {pubbc1}\npriv: {privateWip.decode("utf-8")}')
#     '''8l0m4st3r'''
#     counter.increment()

#     if counter.value() & 2047 == 2047:
#         print(pubbc1)
#         print(f'{counter.getSpeed()}')
#         counter.reset()
#         if bool(False):
#             break


def generator(counter: Counter):
    while True:
        private_seed: bytes = os.urandom(32)
        privateKey: ec.EllipticCurvePrivateKey = ec.derive_private_key(int.from_bytes(private_seed, 'big', signed=False), ec.SECP256K1())

        pubbc1 = gen_bech_public_key(privateKey.public_key().public_numbers())
        match = regex.match(pubbc1[4:])
        if match:
            print('Found one!')
            print(f'{counter.total} since last match')
            counter.total = 0
            print(match)
            privateWip = gen_wip_private_key(private_seed)
            with open(f'{pubbc1}.key', 'w+') as file:
                file.write(f'pub: {pubbc1}\npriv: {privateWip.decode("utf-8")}')
        counter.increment()

        # if counter.value() & 2047 == 2047:
        #     print(pubbc1)
        #     print(f'{counter.getSpeed() / (time.perf_counter() - counter.start)}')
        #     counter.reset()


workers = int(sys.argv[1])


def start():
    # threading.Thread()
    counter = Counter()
    processes: List[Process] = []
    for x in range(workers):
        processes.append(multiprocessing.Process(target=generator, args=(counter,)))
    for x in processes:
        x.start()
    return (counter, processes)


if __name__ == '__main__':
    # threading.Thread(target=start).start()
    counter, processes = start()
    try:
        while True:
            time.sleep(2)
            speed = counter.getSpeed()
            print(f'{speed} addr/sec')
            print(f'{speed/workers} worker addr/sec')
            counter.reset()
    except KeyboardInterrupt:
        for x in processes:
            try:
                x.join()
            except:  # noqa:E722
                pass
        exit()



# EOF
